﻿public enum DataVerbs
{
    Get = 1,
    Post =2,
    Put = 3,
    Delete = 4,
    Generate =5,
    Alternate =6,
    Report=7,
}